#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

// digital pins
#define AMUX_SEL_PIN D4

// analog pins
#define AMUX_ANALOG_PIN A0

// wifi credentials
const char *ssid =  "Woestgaafsecure Gamehall";   // cannot be longer than 32 characters!
const char *pass =  "fantazero";

// MQTT server credentials
const char *mqtt_server = "m23.cloudmqtt.com";
const int mqtt_port = 11961;
const char *mqtt_user = "rpqfpqgw";
const char *mqtt_pass = "VkfECa7-LFNu";
const char *mqtt_client_name = "sam"; // Client connections cant have the same connection name

// init libraries
Adafruit_BME280 bme;
WiFiClient wifi_client;
PubSubClient mqtt_client(mqtt_server, mqtt_port, wifi_client);


// setup analog multiplexer board
void setupAmux() {
	// setup the pin modes
	pinMode(AMUX_ANALOG_PIN, INPUT);
	pinMode(AMUX_SEL_PIN, OUTPUT);
	digitalWrite(AMUX_SEL_PIN, LOW);
}

// in hekto Pacal (hPa)
int getPressure() {
	return bme.readPressure() / 100;
}

// in degree celsius
float getTemperature() {
	return bme.readTemperature();
}

// in percent
float getHumidity() {
	return bme.readHumidity();
}

// setup MQTT
void setupMqtt() {
	// hook callback for incoming messages
	mqtt_client.setCallback(*callback);
}

// gets called when a subscribed MQTT topic arrives
static void callback(char* topic, byte* payload, unsigned int length) {
	Serial.print("MQTT message arrived on topic: ");
	Serial.println((String)topic);
	for (int i = 0; i < length; i++) 
		Serial.print((char)payload[i]);
	Serial.println();
	Serial.println("MQTT message end");
}

// connect to the WIFI network
void connectWifi() {
	Serial.println("Connecting to WiFi: ");
	Serial.println(ssid);
	WiFi.begin(ssid, pass);
}

// connect to the MQTT broker
void connectMqtt() {
	if (!mqtt_client.connected()) {
		Serial.print("Attempting MQTT connection...");
		// Attempt to connect
		if (mqtt_client.connect(mqtt_client_name, mqtt_user, mqtt_pass, mqtt_will_topic, 0, 1, mqtt_will_msg)) {
			Serial.println("connected to MQTT client.");
			// register incoming topics here
			// mqtt_client.subscribe("topic/subtopic");
		} else {
			Serial.print("failed with code ");
			Serial.println(client.state());
		}
	}
}

// returns true if WiFi connection lost
bool wifiDisconnected() {
	return WiFi.status() != WL_CONNECTED;
}

// returns true if MQTT connection lost
bool mqttDisconnected() {
	return !mqtt_client.connected();
}

// send topic to the MQTT broker (accepts integer)
void mqttPublishInt(char *topic, int value) {
	if (client.connected()) {
		char buffer[7];
		dtostrf(value, 7, 0, buffer);
		mqtt_client.publish(topic, buffer);
	}
}

// MAIN
void setup() {
	// setup Serial
	Serial.begin(9600);
	setupAmux();
	setupMqtt();
}

void loop() {
	if (wifiDisconnected()) connectWifi();
	if (mqttDisconnected()) connectMqtt();
	
	mqttPublishInt("topic", 42);
	
	delay(4000); // wait 4 seconds
}

