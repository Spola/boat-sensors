#include <SPI.h>
#include <mcp2515.h>
#include <time.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

struct can_frame msg1a, msg1b, msg4, msg6;
struct can_frame canRes;
struct can_frame canMsg;
struct can_frame lastCanMsg;
MCP2515 mcp2515(D8); // Setze CS Pin

int step = 1;

bool received2a = false;
bool received2b = false;

bool received5a = false;
bool received5b = false;

unsigned long lastMillisSend = 0;        // will store last time
unsigned long lastMillisReceived = 0;        // will store last time
const long interval = 100;           // interval at which to send (milliseconds)

unsigned long lastPublished = 0;
unsigned long lastPublishedPower = 0;
unsigned long lastPublishedTemp = 0;

//Neu
const char* SSID = "SurfenWithSandy";
const char* PSK = "Sandy2018";

// MQTT server credentials
const char *mqtt_server = "192.168.1.1";
const int mqtt_port = 1883;
const char *mqtt_user = "Sandy";
const char *mqtt_pass = "Sandy2018";
const char *mqtt_client_name = "bms"; // Client connections cant have the same connection name


WiFiClient espClient;
PubSubClient client(espClient);
char msgMqtt[50];



void initStep1();
bool initStep2();
void initStep3();
void initStep4();
bool initStep5();
void initStep6();

void setup_wifi();
void reconnect();
void mqttPublishInt(char *topic, int value);
void mqttPublishString(char *topic, char);

void keepBmsAlive();
bool bmsIsAlive();

void printRawMsg(can_frame msg);
void decodeMsg(can_frame msg);

unsigned int twoBytes2int(unsigned char a, unsigned char b);
double threeBytes2double(unsigned char a, unsigned char b, unsigned char c);
