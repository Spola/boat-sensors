/*
This porgram converts raw input value from
*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// digital pins
#define AMUX_SEL_PIN D0

// analog pins
#define AMUX_ANALOG_PIN A0

// wifi credentials
const char *ssid =  "SurfenWithSandy";   // cannot be longer than 32 characters!
const char *pass =  "Sandy2018";

// MQTT server credentials
const char *mqtt_server = "192.168.1.1";
const int mqtt_port = 1883;
const char *mqtt_user = "user";
const char *mqtt_pass = "password";
const char *mqtt_client_name = "windmesser"; // Client connections cant have the same connection name

// init libraries
WiFiClient wifi_client;
PubSubClient mqtt_client(mqtt_server, mqtt_port, wifi_client);

// setup analog multiplexer board
void setupAmux() {
	// setup the pin modes
	pinMode(AMUX_ANALOG_PIN, INPUT);
	pinMode(AMUX_SEL_PIN, OUTPUT);
	digitalWrite(AMUX_SEL_PIN, LOW);
}

// connect to the WIFI network
void connectWifi() {
	Serial.println("Connecting to WiFi: ");
	Serial.println(ssid);
	WiFi.begin(ssid, pass);
}

// connect to the MQTT broker
void connectMqtt() {
	if (!mqtt_client.connected()) {
		Serial.print("Attempting MQTT connection...");
		// Attempt to connect
		if (mqtt_client.connect(mqtt_client_name, mqtt_user, mqtt_pass)) {
			Serial.println("connected to MQTT client.");
			// register incoming topics here
			// mqtt_client.subscribe("topic/subtopic");
		} else {
			Serial.print("failed with code ");
			Serial.println(mqtt_client.state());
		}
	}
}

// returns true if WiFi connection lost
bool wifiDisconnected() {
	return WiFi.status() != WL_CONNECTED;
}

// returns true if MQTT connection lost
bool mqttDisconnected() {
	return !mqtt_client.connected();
}

// send topic to the MQTT broker (accepts integer)
void mqttPublishInt(char *topic, int value) {
	if (mqtt_client.connected()) {
		char buffer[7];
		dtostrf(value, 7, 0, buffer);
		mqtt_client.publish(topic, buffer);
	}
}

// MAIN
void setup() {
	// setup Serial
	Serial.begin(9600);
	setupAmux();
}

void loop() {
	if (wifiDisconnected()) connectWifi();
	if (mqttDisconnected()) connectMqtt();

  // select wind speed
	digitalWrite(AMUX_SEL_PIN, LOW);
  delay(20);

  int sensorValue = analogRead(A0);
  float outvoltage = sensorValue * (5.0 / 1023.0);
  //Serial.print("outvoltage speed: ");
  //Serial.print(outvoltage);
  //Serial.println("V");
  // The level of wind speed is proportional to the output voltage.
  int speed = 6 * outvoltage;
  //Serial.print("wind speed is ");
  //Serial.print(speed);
  //Serial.println(" m/s");
  mqttPublishInt("windspeed", speed);

  // select wind direction
	digitalWrite(AMUX_SEL_PIN, HIGH);
  delay(20);
  sensorValue = analogRead(A0);
  Serial.print("raw direction: ");
  Serial.println(sensorValue);
  int direction = sensorValue / (940./360.);
  int offset = 71;
  direction = (direction - offset) % 360;
  if (direction < 0) direction = 360 + direction;
  Serial.print("wind direction is ");
  Serial.print(direction);
  Serial.println(" deg");

  //mqttPublishInt("winddirection", direction);

  delay(200); // interval
}
